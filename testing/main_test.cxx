//
// File mock_amr2D_3mat_test.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
/*----------------------------------------------------------------------------*/
#include <gtest/gtest.h>
/*----------------------------------------------------------------------------*/
#include "mock_amr2D_test.h"
#include "mock_amr2D_3mat_test.h"
/*----------------------------------------------------------------------------*/
int main(int argc, char ** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  mock_amr2D_test();
  mock_amr2D_3mat_test();
  return RUN_ALL_TESTS();
}
/*----------------------------------------------------------------------------*/

