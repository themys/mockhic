//
// File mock_amr2D_3mat_test.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
/*----------------------------------------------------------------------------*/
#ifndef MOCKHIC_MOCK_AMR2D_3MAT_TEST_H
#define MOCKHIC_MOCK_AMR2D_3MAT_TEST_H
/*----------------------------------------------------------------------------*/
#include <string>
/*----------------------------------------------------------------------------*/
#include <gtest/gtest.h>
/*----------------------------------------------------------------------------*/
#include "mockhic/mockhic.hpp"
HIC_USE
/*----------------------------------------------------------------------------*/
void mock_amr2D_3mat_test()
{
  MOCKHIC_STAT_TIC_MET("", "mock_amr2D_3mat_test", "")
  HIc_Api api = HIc_Api("myApi", "stdCommon"); // add Xavier
  HIc_Init_Standard_Services(api);
  HIc_Init_Standard_Site(api, "");
  std::string fileName;
  if (getenv("MOCKHIC_DATA_DIR"))
  {
    MOCKHIC_STAT("USE environment variable MOCKHIC_DATA_DIR");
    fileName = std::string(getenv("MOCKHIC_DATA_DIR")) + "one_amr2D_3mat-.json";
  }
  else
  {
    MOCKHIC_STAT("not environment variable MOCKHIC_DATA_DIR");
    fileName = std::string(MOCKHIC_DATA_DIR) + "one_amr2D_3mat-.json";
  }
  HIc_String baseName;
  api.getBaseNameFromFileName(fileName, baseName);

  std::string path = fileName;
  size_t slash = path.find_last_of('/');
  if (slash != std::string::npos)
  {
    path = path.substr(0, slash);
  }

  HIc_Base base = api.createBase("myBase");
  base.setMode("read");
  base.setItemConf("bd_name", baseName);
  base.setItemConf("multi_period", baseName); // TODO ???
  base.setItemConf("read_dir", path);
  base.open();
  ASSERT_TRUE(base.isOpen());

  std::vector<double> lst_temps;
  base.getTimeList(lst_temps);
  ASSERT_EQ(lst_temps.size(), 2);

  {
    HIc_Ctx ctx = base.getCtxPar(lst_temps[lst_temps.size() - 1], 0);
    ctx.open();

#define GET_INFO(name)                                                                             \
  try                                                                                              \
  {                                                                                                \
    HIc_Obj o_tmp = o_root.getAttrIsExist(name);                                                   \
    if (!o_tmp.isNull())                                                                           \
    {                                                                                              \
      o_tmp.getVal(infoBase);                                                                      \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      infoBase = "";                                                                               \
    }                                                                                              \
  }                                                                                                \
  catch (HERCULE::Exception & e)                                                                   \
  {                                                                                                \
    MOCKHIC_STAT_WARNING(                                                                          \
      "Cette attribut de la base " << name << " n'est pas disponible !" << name);                  \
    std::cerr << e.what() << std::endl;                                                            \
  }

    HIc_Obj o_root = ctx.getRoot();
    ASSERT_EQ(o_root.getName(), ".");
    ASSERT_EQ(o_root.getFullName(), ".");

    std::string infoBase;
    GET_INFO("utilisateur.nom");
    ASSERT_EQ(infoBase, "moa");
    GET_INFO("date.nom");
    ASSERT_EQ(infoBase, "Tue Nov 10 12:36:27 CET 2020");
    GET_INFO("outil.nom");
    ASSERT_EQ(infoBase, "tools");
    GET_INFO("outil.version");
    ASSERT_EQ(infoBase, "");
    GET_INFO("etude.nom");
    ASSERT_EQ(infoBase, "");
#undef GET_INFO
    {
      HIc_Obj o_root = ctx.getRoot();
      HIc_Obj o_tmp = o_root.getAttrIsExist("contenu.numero_cycle");
      long int cycle = 0;
      o_tmp.getVal(cycle);
      char tmp[32];
      sprintf(tmp, "%ld", cycle);
      ASSERT_EQ(cycle, 42);
    }
    std::vector<HIc_Obj> o_maillages = ctx.search("Maillage");
    ASSERT_EQ(o_maillages.size(), 1);
    ASSERT_EQ(o_maillages[0].getName(), "mesh");
    ASSERT_EQ(o_maillages[0].getFullName(), ".mesh");

    HIc_Obj o_maillage = o_maillages[0];

    int dimension = 3;
    ASSERT_EQ(o_maillage.getTypeName(), "Maillage-2D-AMR-S-Ortho-Regulier");
    if (o_maillage.getTypeName() == "Maillage-2D-AMR-S-Ortho-Regulier")
    {
      dimension = 2;
    }
    ASSERT_EQ(dimension, 2);

    std::vector<double> bounds(6);
    o_maillage.getAttrIsExist("globalPos.coordMin.X").getVal(&bounds[0]);
    o_maillage.getAttrIsExist("globalPos.coordMax.X").getVal(&bounds[1]);
    o_maillage.getAttrIsExist("globalPos.coordMin.Y").getVal(&bounds[2]);
    o_maillage.getAttrIsExist("globalPos.coordMax.Y").getVal(&bounds[3]);
    if (false)
    {
      o_maillage.getAttrIsExist("globalPos.coordMin.Z").getVal(&bounds[4]);
      o_maillage.getAttrIsExist("globalPos.coordMax.Z").getVal(&bounds[5]);
    }

    std::vector<unsigned int> gridsize(3);
    o_maillage.getAttrIsExist("globalPos.nbMailleI").getVal(&gridsize[0]);
    o_maillage.getAttrIsExist("globalPos.nbMailleJ").getVal(&gridsize[1]);
    o_maillage.getAttrIsExist("globalPos.nbMailleK").getVal(&gridsize[2]);

    long typeDeRaffinement;
    o_maillage.getAttrIsExist("typeDeRaffinement").getVal(&typeDeRaffinement);
    ASSERT_EQ(typeDeRaffinement, 2);

    long niveauMax;
    o_maillage.getAttrIsExist("nbLevels").getVal(&niveauMax);
    ASSERT_EQ(niveauMax, 6);

    std::vector<long> nbEltPerLevel(niveauMax);
    o_maillage.getAttrIsExist("nbElementsPerLevel").getVal(nbEltPerLevel.data(), niveauMax);

    std::vector<long> position(nbEltPerLevel[0] * dimension);
    o_maillage.getAttrIsExist("globalPos.position")
      .getVal(position.data(), nbEltPerLevel[0] * dimension);

    long nbElements;
    o_maillage.getAttrIsExist("nbElements").getVal(&nbElements);
    ASSERT_EQ(nbElements, 98);

    std::vector<unsigned char> isParent;
    isParent.resize(nbElements);
    o_maillage.getAttrIsExist("isParent_i1").getVal(isParent.data(), nbElements);
    ASSERT_EQ(isParent.size(), 98);

    HIc_Obj o_elements = o_maillage.getAttrIsExist("elements");

    HIc_Obj o_ids = o_elements.getAttrIsExist("id");
    std::vector<long> ids;
    if (!o_ids.isNull())
    {
      ids.resize(nbElements);
      o_ids.getVal(ids.data(), nbElements);
      ASSERT_EQ(ids.size(), 98);
    }

    std::map<std::string, std::vector<long> > resu;
    resu["MatMere"].resize(2);
    resu["MatMere"][0] = 17;  // nbSel
    resu["MatMere"][1] = 205; // Sum(sel)
    resu["all1"].resize(2);
    resu["all1"][0] = -1; // nbSel
    resu["all1"][1] = 0;  // Sum(sel)
    resu["all2"].resize(2);
    resu["all2"][0] = -1; // nbSel
    resu["all2"][1] = 0;  // Sum(sel)
    resu["znothing"].resize(2);
    resu["znothing"][0] = 0; // nbSel
    resu["znothing"][1] = 0; // Sum(sel)
    resu["mat1"].resize(2);
    resu["mat1"][0] = 14;  // nbSel
    resu["mat1"][1] = 280; // Sum(sel)

    std::map<std::string, std::map<std::string, std::vector<long> > > resuGrd;
    resuGrd["global"]["herfield"].resize(2);
    resuGrd["global"]["herfield"][0] = 98;  // nbSel
    resuGrd["global"]["herfield"][1] = 393; // Sum
    resuGrd["global"]["level"].resize(2);
    resuGrd["global"]["level"][0] = 98;  // nbSel
    resuGrd["global"]["level"][1] = 232; // Sum
    resuGrd["global"]["rlevel"].resize(2);
    resuGrd["global"]["rlevel"][0] = 98;  // nbSel
    resuGrd["global"]["rlevel"][1] = 356; // Sum

    resuGrd["MatMere"]["herfield"].resize(2);
    resuGrd["MatMere"]["herfield"][0] = 17;  // nbSel
    resuGrd["MatMere"]["herfield"][1] = 154; // Sum
    resuGrd["MatMere"]["level"].resize(2);
    resuGrd["MatMere"]["level"][0] = 17; // nbSel
    resuGrd["MatMere"]["level"][1] = 13; // Sum
    resuGrd["MatMere"]["rlevel"].resize(2);
    resuGrd["MatMere"]["rlevel"][0] = 17; // nbSel
    resuGrd["MatMere"]["rlevel"][1] = 89; // Sum
    resuGrd["all1"]["herfield"].resize(2);
    resuGrd["all1"]["herfield"][0] = 98;  // nbSel
    resuGrd["all1"]["herfield"][1] = 393; // Sum
    resuGrd["all1"]["level"].resize(2);
    resuGrd["all1"]["level"][0] = 98;  // nbSel
    resuGrd["all1"]["level"][1] = 232; // Sum
    resuGrd["all1"]["rlevel"].resize(2);
    resuGrd["all1"]["rlevel"][0] = 98;  // nbSel
    resuGrd["all1"]["rlevel"][1] = 356; // Sum
    resuGrd["all2"]["herfield"].resize(2);
    resuGrd["all2"]["herfield"][0] = 98;  // nbSel
    resuGrd["all2"]["herfield"][1] = 393; // Sum
    resuGrd["all2"]["level"].resize(2);
    resuGrd["all2"]["level"][0] = 98;  // nbSel
    resuGrd["all2"]["level"][1] = 232; // Sum
    resuGrd["all2"]["rlevel"].resize(2);
    resuGrd["all2"]["rlevel"][0] = 98;  // nbSel
    resuGrd["all2"]["rlevel"][1] = 356; // Sum
    resuGrd["znothing"]["herfield"].resize(2);
    resuGrd["znothing"]["herfield"][0] = 0; // nbSel
    resuGrd["znothing"]["herfield"][1] = 0; // Sum
    resuGrd["znothing"]["level"].resize(2);
    resuGrd["znothing"]["level"][0] = 0; // nbSel
    resuGrd["znothing"]["level"][1] = 0; // Sum
    resuGrd["znothing"]["rlevel"].resize(2);
    resuGrd["znothing"]["rlevel"][0] = 0; // nbSel
    resuGrd["znothing"]["rlevel"][1] = 0; // Sum
    resuGrd["mat1"]["herfield"].resize(2);
    resuGrd["mat1"]["herfield"][0] = 14;  // nbSel
    resuGrd["mat1"]["herfield"][1] = 106; // Sum
    resuGrd["mat1"]["level"].resize(2);
    resuGrd["mat1"]["level"][0] = 14; // nbSel
    resuGrd["mat1"]["level"][1] = 14; // Sum
    resuGrd["mat1"]["rlevel"].resize(2);
    resuGrd["mat1"]["rlevel"][0] = 14; // nbSel
    resuGrd["mat1"]["rlevel"][1] = 70; // Sum

    {
      std::string mil_name = "global";

      std::vector<HIc_Obj> grandeurs = o_elements.search("Grandeur");
      for (auto& o_grandeur : grandeurs)
      {
        // std::cerr << "ICI" << mil_name << std::endl;
        std::string field_name = o_grandeur.getName();
        // std::cerr << "ICI" << mil_name << ":" << field_name << std::endl;
        std::string field_type = o_grandeur.getTypeName();
        // std::cerr << "ICI" << mil_name << ":" << field_name << " " << field_type << std::endl;
        long nb = o_grandeur.getNbVals();
        // std::cerr << "ICI" << mil_name << ":" << field_name << " " << field_type << " #" << nb
        //          << std::endl;
        ASSERT_EQ(nb, resuGrd[mil_name.c_str()][field_name.c_str()][0]);
        ASSERT_EQ(o_grandeur.getAttrIsExist("val").getNbVals(),
          resuGrd[mil_name.c_str()][field_name.c_str()][0]);
        if (field_type == "GrandeurScalaireEntier")
        {
          std::cerr << "GrandeurScalaireEntier in progress" << std::endl;
          std::vector<long> vals(nb, 0);
          o_grandeur.getAttrIsExist("val").getVal(vals.data(), vals.size());
          long sum = 0;
          for (const auto& val : vals)
          {
            sum += val;
          }
          std::cerr << "GrandeurScalaireEntier checks" << std::endl;
          ASSERT_EQ(sum, resuGrd[mil_name.c_str()][field_name.c_str()][1]);
          std::cerr << "GrandeurScalaireEntier checked" << std::endl;
        }
        else if (field_type == "GrandeurScalaireFlottant")
        {
          std::cerr << "GrandeurScalaireFlottant in progress" << std::endl;
          std::vector<float> vals(nb, 0.);
          o_grandeur.getAttrIsExist("val").getVal(vals.data(), vals.size());
          double sum = 0;
          for (const auto& val : vals)
          {
            sum += val;
          }
          std::cerr << "GrandeurScalaireFlottant checks" << std::endl;
          ASSERT_EQ(long(sum), resuGrd[mil_name.c_str()][field_name.c_str()][1]);
          std::cerr << "GrandeurScalaireFlottant checked" << std::endl;
        }
        else
        {
          ASSERT_TRUE(false);
        }
        ASSERT_EQ(nb, resuGrd[mil_name.c_str()][field_name.c_str()][0]);
      }
    }

    std::vector<HIc_Obj> milieux = o_elements.search("Milieu");
    for (auto& o_milieu : milieux)
    {
      std::string mil_name = o_milieu.getName();
      const long* sel = nullptr;
      long nbSel = 0;
      HIc_Support sup = o_milieu.getSupport();
      HIc_SupportDesc
        supDesc; // Feature Hercule: The lifespan of "sel"" is related to that of "supDesc"
      if (sup.isNull())
      {
        nbSel = -1;
        ASSERT_EQ(-1, resu[mil_name.c_str()][0]);
      }
      else
      {
        supDesc = sup.getSupportDesc(0);
        if (supDesc.isASel())
        {
          nbSel = supDesc.getNbSel();
          sel = supDesc.getSel();
        }
        else
        {
          nbSel = -1;
        }
      }
      ASSERT_EQ(nbSel, resu[mil_name.c_str()][0]);
      if (nbSel == -1)
      {
        ASSERT_EQ(0, resu[mil_name.c_str()][1]);
      }
      else
      {
        long sumSel = 0;
        for (long i = 0; i < nbSel; ++i)
        {
          sumSel += sel[i];
        }
        ASSERT_EQ(sumSel, resu[mil_name.c_str()][1]);
      }

      std::vector<HIc_Obj> grandeurs = o_milieu.search("Grandeur");
      for (auto& o_grandeur : grandeurs)
      {
        // std::cerr << "ICI" << mil_name << std::endl;
        std::string field_name = o_grandeur.getName();
        // std::cerr << "ICI" << mil_name << ":" << field_name << std::endl;
        std::string field_type = o_grandeur.getTypeName();
        // std::cerr << "ICI" << mil_name << ":" << field_name << " " << field_type << std::endl;
        long nb = o_grandeur.getNbVals();
        // std::cerr << "ICI" << mil_name << ":" << field_name << " " << field_type << " #" << nb
        //          << std::endl;
        ASSERT_EQ(nb, resuGrd[mil_name.c_str()][field_name.c_str()][0]);
        ASSERT_EQ(o_grandeur.getAttrIsExist("val").getNbVals(),
          resuGrd[mil_name.c_str()][field_name.c_str()][0]);
        if (field_type == "GrandeurScalaireEntier")
        {
          std::cerr << "GrandeurScalaireEntier in progress" << std::endl;
          std::vector<long> vals(nb, 0);
          o_grandeur.getAttrIsExist("val").getVal(vals.data(), vals.size());
          long sum = 0;
          for (const auto& val : vals)
          {
            sum += val;
          }
          std::cerr << "GrandeurScalaireEntier checks" << std::endl;
          ASSERT_EQ(sum, resuGrd[mil_name.c_str()][field_name.c_str()][1]);
          std::cerr << "GrandeurScalaireEntier checked" << std::endl;
        }
        else if (field_type == "GrandeurScalaireFlottant")
        {
          std::cerr << "GrandeurScalaireFlottant in progress" << std::endl;
          std::vector<float> vals(nb, 0.);
          o_grandeur.getAttrIsExist("val").getVal(vals.data(), vals.size());
          double sum = 0;
          for (const auto& val : vals)
          {
            sum += val;
          }
          std::cerr << "GrandeurScalaireFlottant checks" << std::endl;
          ASSERT_EQ(long(sum), resuGrd[mil_name.c_str()][field_name.c_str()][1]);
          std::cerr << "GrandeurScalaireFlottant checked" << std::endl;
        }
        else
        {
          ASSERT_TRUE(false);
        }
        ASSERT_EQ(nb, resuGrd[mil_name.c_str()][field_name.c_str()][0]);
      }
    }
    ctx.close();
  }

  bool multiPeriode = base.isCstNbDomains();
  for (const auto& it : lst_temps)
  {
    long nbDomains = base.getNbDomains(it);
    for (long idom = 0; idom < nbDomains; ++idom)
    {
      HIc_Ctx ctx = base.getCtxPar(it, idom);
      ctx.open();

      ctx.close();
    }
  }
  base.close();
  MOCKHIC_STAT_TAC
}

#endif // MOCKHIC_MOCK_AMR2D_3MAT_TEST_H
