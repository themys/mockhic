//
// File mock_amr2D_3mat_test.h
//
// Created by Jacques-Bernard Lekien
// Copyright (c) 2020 CEA, DAM, DIF, F-91297 Arpajon, France.
// All rights reserved.
//
/*----------------------------------------------------------------------------*/
#ifndef MOCKHIC_MOCK_AMR2D_TEST_H
#define MOCKHIC_MOCK_AMR2D_TEST_H
/*----------------------------------------------------------------------------*/
#include <string>
/*----------------------------------------------------------------------------*/
#include <gtest/gtest.h>
/*----------------------------------------------------------------------------*/
#include "mockhic/mockhic.hpp"
HIC_USE
/*----------------------------------------------------------------------------*/
void mock_amr2D_test()
{
  MOCKHIC_STAT_TIC_MET("", "mock_amr2D_test", "")
  HIc_Api api = HIc_Api("myApi", "stdCommon"); // add Xavier
  HIc_Init_Standard_Services(api);
  HIc_Init_Standard_Site(api, "");
  std::string fileName;
  if(getenv("MOCKHIC_DATA_DIR")) {
    MOCKHIC_STAT("USE environment variable MOCKHIC_DATA_DIR");
    fileName = std::string (getenv("MOCKHIC_DATA_DIR"))+ "one_amr2D-.json";
  } else   {
    MOCKHIC_STAT("not environment variable MOCKHIC_DATA_DIR");
    fileName = std::string(MOCKHIC_DATA_DIR)+ "one_amr2D-.json";
  }
  HIc_String baseName;
  api.getBaseNameFromFileName(fileName, baseName);

  std::string path = fileName;
  size_t slash = path.find_last_of('/');
  if (slash != std::string::npos)
  {
    path = path.substr(0, slash);
  }

  HIc_Base base = api.createBase("myBase");
  base.setMode("read");
  base.setItemConf("bd_name", baseName);
  base.setItemConf("multi_period", baseName); // TODO ???
  base.setItemConf("read_dir", path);
  base.open();
  ASSERT_TRUE(base.isOpen());

  std::vector<double> lst_temps;
  base.getTimeList(lst_temps);
  ASSERT_EQ(lst_temps.size(), 2);

  {
    HIc_Ctx ctx = base.getCtxPar(lst_temps[lst_temps.size() - 1], 0);
    ctx.open();

#define GET_INFO(name)                                                                             \
  try                                                                                              \
  {                                                                                                \
    HIc_Obj o_tmp = o_root.getAttrIsExist(name);                                                     \
    if (!o_tmp.isNull())                                                                           \
    {                                                                                              \
      o_tmp.getVal(infoBase);                                                                      \
    }                                                                                              \
    else                                                                                           \
    {                                                                                              \
      infoBase = "";                                                                               \
    }                                                                                              \
  }                                                                                                \
  catch (HERCULE::Exception & e)                                                                   \
  {                                                                                                \
    MOCKHIC_STAT_WARNING("Cette attribut de la base " << name << " n'est pas disponible !" << name);       \
    std::cerr << e.what() << std::endl;                                                            \
  }

    HIc_Obj o_root = ctx.getRoot();
    ASSERT_EQ(o_root.getName(), ".");
    ASSERT_EQ(o_root.getFullName(), ".");

    std::string infoBase;
    GET_INFO("utilisateur.nom");
    ASSERT_EQ(infoBase, "moa");
    GET_INFO("date.nom");
    ASSERT_EQ(infoBase, "Tue Nov 10 12:36:27 CET 2020");
    GET_INFO("outil.nom");
    ASSERT_EQ(infoBase, "tools");
    GET_INFO("outil.version");
    ASSERT_EQ(infoBase, "");
    GET_INFO("etude.nom");
    ASSERT_EQ(infoBase, "");
#undef GET_INFO
    {
      HIc_Obj o_root = ctx.getRoot();
      HIc_Obj o_tmp = o_root.getAttrIsExist("contenu.numero_cycle");
      long int cycle = 0;
      o_tmp.getVal(cycle);
      char tmp[32];
      sprintf(tmp, "%ld", cycle);
      ASSERT_EQ(cycle, 42);
    }
    std::vector<HIc_Obj> o_maillages = ctx.search("Maillage");
    ASSERT_EQ(o_maillages.size(), 1);
    ASSERT_EQ(o_maillages[0].getName(), "mesh");
    ASSERT_EQ(o_maillages[0].getFullName(), ".mesh");

    HIc_Obj o_maillage = o_maillages[0];

    int dimension = 3;
    ASSERT_EQ(o_maillage.getTypeName(), "Maillage-2D-AMR-S-Ortho-Regulier");
    if(o_maillage.getTypeName() == "Maillage-2D-AMR-S-Ortho-Regulier") {dimension = 2;}
    ASSERT_EQ(dimension, 2);

    std::vector<double> bounds(6);
    o_maillage.getAttrVal("globalPos.coordMin.X", &bounds[0]);
    o_maillage.getAttrVal("globalPos.coordMax.X", &bounds[1]);
    o_maillage.getAttrVal("globalPos.coordMin.Y", &bounds[2]);
    o_maillage.getAttrVal("globalPos.coordMax.Y", &bounds[3]);
    if(false) {
      o_maillage.getAttrVal("globalPos.coordMin.Z", &bounds[4]);
      o_maillage.getAttrVal("globalPos.coordMax.Z", &bounds[5]);
    }

    std::vector<unsigned int> gridsize(3);
    o_maillage.getAttrVal("globalPos.nbMailleI", &gridsize[0]);
    o_maillage.getAttrVal("globalPos.nbMailleJ", &gridsize[1]);
    o_maillage.getAttrVal("globalPos.nbMailleK", &gridsize[2]);

    long typeDeRaffinement;
    o_maillage.getAttrVal("typeDeRaffinement", &typeDeRaffinement);
    ASSERT_EQ(typeDeRaffinement, 2);

    long niveauMax;
    o_maillage.getAttrVal("nbLevels", &niveauMax);
    ASSERT_EQ(niveauMax, 6);

    std::vector<long> nbEltPerLevel(niveauMax);
    o_maillage.getAttrVal("nbElementsPerLevel", nbEltPerLevel.data(), niveauMax);

    std::vector<long> position(nbEltPerLevel[0] * dimension);
    o_maillage.getAttrVal("globalPos.position", position.data(), nbEltPerLevel[0] * dimension);

    long nbElements;
    o_maillage.getAttrVal("nbElements", &nbElements);
    ASSERT_EQ(nbElements, 98);

    std::vector<unsigned char> isParent;
    isParent.resize(nbElements);
    o_maillage.getAttrVal("isParent_i1", isParent.data(), nbElements);
    ASSERT_EQ(isParent.size(), 98);

    HIc_Obj o_elements = o_maillage.getAttr("elements");

    HIc_Obj o_ids = o_elements.getAttrIsExist("id");
    std::vector<long> ids;
    if (!o_ids.isNull())
    {
      ids.resize(nbElements);
      o_ids.getVal(ids.data(), nbElements);
      ASSERT_EQ(ids.size(), 98);
    }

    std::vector<HIc_Obj> grandeurs = o_elements.search("Grandeur");
    for (auto& o_grandeur : grandeurs)
    {
    }

    std::vector<HIc_Obj> milieux = o_elements.search("Milieu");
    for (auto& o_milieu : milieux)
    {
      std::vector<HIc_Obj> grandeurs = o_milieu.search("Grandeur");
      for (auto& o_grandeur : grandeurs)
      {
      }

    }
    ctx.close();
  }

  bool multiPeriode = base.isCstNbDomains();
  for (const auto& it : lst_temps)
  {
    long nbDomains = base.getNbDomains(it);
    for (long idom = 0; idom < nbDomains; ++idom)
    {
      HIc_Ctx ctx = base.getCtxPar(it, idom);
      ctx.open();

      ctx.close();
    }
  }
  base.close();
  MOCKHIC_STAT_TAC
}

#endif // MOCKHIC_MOCK_AMR2D_TEST_H
