/*
 * mockhic_macro.hpp
 *
 *  CEA, DAM, DIF, F-91297 Arpajon, France
 */

#ifndef _MOCKHIC_MACRO_HPP_
#define _MOCKHIC_MACRO_HPP_

//--------------------------------------------------------------------------------------------------
#define MOCKHIC_COMMUN_MACRO_STAT

//--------------------------------------------------------------------------------------------------
#ifdef MOCKHIC_COMMUN_MACRO_STAT

#define MOCKHIC_COMMUN_MACRO_STAT_OUTPUT std::cerr

#include <chrono>
#include <ctime>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <thread>
#include <vector>

#define MOCKHIC_STAT_CND true

class MOCKHIC_STAT_CLASS
{
protected:
  std::string m_stat_para = "";

public:
  void setStatPara(const std::string& _stat_para) { m_stat_para = _stat_para; }
  const std::string getStatPara(void) const { return m_stat_para; }
};

// ajouter une option chatter
#define MOCKHIC_STAT_TIC(clss, fct)                                                                \
  std::string stat_class_fct = std::string(clss) + "::" + fct;                                     \
  std::string _stat_para = getStatPara();                                                          \
  auto stat_class_fct_tic = std::chrono::system_clock::now();                                      \
  MOCKHIC_COMMUN_MACRO_STAT_OUTPUT << "IN  [" << stat_class_fct << _stat_para << "]" << std::endl;

// la version pour methode isolee, sans stat_para
#define MOCKHIC_STAT_TIC_MET(clss, fct, stat_para)                                                 \
  std::string stat_class_fct = std::string(clss) + "::" + fct;                                     \
  std::string _stat_para = stat_para;                                                              \
  auto stat_class_fct_tic = std::chrono::system_clock::now();                                      \
  MOCKHIC_COMMUN_MACRO_STAT_OUTPUT << "IN  [" << stat_class_fct << _stat_para << "]" << std::endl;

#define MOCKHIC_STAT_TAC                                                                           \
  {                                                                                                \
    auto stat_class_fct_tac = std::chrono::system_clock::now();                                    \
    std::chrono::duration<double> elapsed_seconds = stat_class_fct_tac - stat_class_fct_tic;       \
    MOCKHIC_COMMUN_MACRO_STAT_OUTPUT << "OUT [" << stat_class_fct << _stat_para                    \
                                     << "] TimeElapse:" << elapsed_seconds.count() << " s"         \
                                     << std::endl;                                                 \
  }

#define MOCKHIC_STAT(msg)                                                                          \
  MOCKHIC_COMMUN_MACRO_STAT_OUTPUT << "    [" << stat_class_fct << _stat_para << "] " << msg       \
                                   << std::endl;

#define MOCKHIC_STAT_MIN_MAX(type, vals, nbVals, msg)                                              \
  if (MOCKHIC_STAT_CND && nbVals)                                                                  \
  {                                                                                                \
    type min = vals[0];                                                                            \
    type max = vals[0];                                                                            \
    for (long int i = 0; i < nbVals; ++i)                                                          \
    {                                                                                              \
      if (vals[i] < min)                                                                           \
        min = vals[i];                                                                             \
      if (max < vals[i])                                                                           \
        max = vals[i];                                                                             \
    }                                                                                              \
    MOCKHIC_STAT(msg << " nb:" << nbVals << " [min: " << min << ", max: " << max << "]")           \
  }

#define MOCKHIC_STAT_WARNING(msg)                                                                  \
  std::cerr << "### WARNING ### [" << stat_class_fct << _stat_para << "] " << msg << std::endl;

#define MOCKHIC_STAT_ASSERT(cnd, msg)                                                              \
  if (!(cnd))                                                                                      \
  {                                                                                                \
    MOCKHIC_STAT_WARNING(msg);                                                                     \
    exit(1);                                                                                       \
  }

// TODO C++ is bullshit why don't do std::to_string(std::this_thread::get_id()) ?
#define MOCKHIC_STAT_PARA(idProc, nbProc)                                                          \
  if (getStatPara().size() == 0)                                                                   \
  {                                                                                                \
    std::ostringstream oss;                                                                        \
    oss << std::this_thread::get_id() << std::endl;                                                \
    setStatPara("@" + std::to_string(idProc) + "/" + std::to_string(nbProc) + " " + oss.str());    \
    _stat_para = getStatPara();                                                                    \
    MOCKHIC_STAT("");                                                                              \
  }

#define MOCKHIC_STAT_RETURN(cnd, msg)                                                              \
  {                                                                                                \
    MOCKHIC_STAT(msg)                                                                              \
    MOCKHIC_STAT_TAC;                                                                              \
    return cnd;                                                                                    \
  }

//--------------------------------------------------------------------------------------------------
#else

#include <iostream>
#include <string>

class MOCKHIC_STAT_CLASS
{
public:
  void setStatPara(const std::string& _stat_para) {}
  const std::string getStatPara(void) const { return ""; }
};

#define MOCKHIC_STAT_CND false
#define MOCKHIC_STAT_TIC(clss, fct)
#define MOCKHIC_STAT_TAC
#define MOCKHIC_STAT(msg)
#define MOCKHIC_STAT_MIN_MAX(type, vals, nbVals, msg)
#define MOCKHIC_STAT_WARNING(msg) std::cerr << "WARNING " << msg << std::endl;
#define MOCKHIC_STAT_ASSERT(cnd, msg)                                                              \
  if (!(cnd))                                                                                      \
  {                                                                                                \
    MOCKHIC_STAT_WARNING(msg);                                                                     \
    exit(1);                                                                                       \
  }
#define MOCKHIC_STAT_PARA(idProc, nbProc)
#include MOCKHIC_STAT_RETURN(cnd) return cnd;
//--------------------------------------------------------------------------------------------------

#endif // MOCKHIC_COMMUN_MACRO_STAT

#endif // _MOCKHIC_MACRO_HPP_