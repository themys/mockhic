#ifndef _MOCKHIC_HPP_
#define _MOCKHIC_HPP_

#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "mockhic_macro.hpp"

#include "json.hpp"
using json = nlohmann::json;

namespace Mock_HIc
{
typedef std::exception Exception;
typedef std::string HIc_String;
typedef HIc_String HERString;

class HIc_SupportDesc;
class HIc_Support;
class HIc_Obj;
class HIc_Ctx;
class HIc_Base;
class HIc_Api;

class HIc_Describe
{
public:
  const json& m_jdatas;
  int m_time;
  int m_numDomain;
  std::string m_fullname;
  std::size_t m_i_support_desc;

  HIc_Describe(const json& _jdatas)
    : m_jdatas(_jdatas)
  {
  }
  HIc_Describe(const HIc_Describe& _datas)
    : m_jdatas(_datas.m_jdatas)
    , m_time(_datas.m_time)
    , m_numDomain(_datas.m_numDomain)
    , m_fullname(_datas.m_fullname)
    , m_i_support_desc(_datas.m_i_support_desc)
  {
  }
  HIc_Describe(const HIc_Describe& _datas, const std::string& _fullname)
    : m_jdatas(_datas.m_jdatas)
    , m_time(_datas.m_time)
    , m_numDomain(_datas.m_numDomain)
    , m_fullname(_fullname)
  {
  }
};

class HIc_SupportDesc : public STAT_CLASS
{
private:
  const HIc_Describe m_datas;

public:
  HIc_SupportDesc(const HIc_Describe& datas)
    : m_datas(datas)
  {
  }
  virtual ~HIc_SupportDesc() {}

  bool isNull() { return true; }

  bool isASel(void) const { return true; }
  std::size_t getNbSel(void) const { return 0; }
  const long* getSel(void) { return nullptr; }
};

class HIc_Support : public STAT_CLASS
{
private:
  const HIc_Describe m_datas;

public:
  HIc_Support(const HIc_Describe& datas)
    : m_datas(datas)
  {
  }
  virtual ~HIc_Support() {}

  bool isNull() { return true; }

  std::size_t getNbSupportDesc(void) { return 0; }
  HIc_SupportDesc getSupportDesc(std::size_t _i)
  {
    HIc_Describe datas(m_datas);
    datas.m_i_support_desc = _i;
    return HIc_SupportDesc(datas);
  }
};

class HIc_Obj : public STAT_CLASS
{
private:
  const HIc_Describe m_datas;

public:
  HIc_Obj(const HIc_Describe datas)
    : m_datas(datas)
  {
  }
  virtual ~HIc_Obj(void) {}

  bool isNull() { return m_datas.m_fullname == ""; }

  const HIc_String getName(void)
  {
    if (m_datas.m_fullname == ".")
    {
      return ".";
    }
    return m_datas.m_fullname.substr(m_datas.m_fullname.find_last_of('.') + 1);
  }
  const HIc_String getTypeName(void) { return ""; }
  const HIc_String getFullName(void) { return m_datas.m_fullname; }

  HIc_Support getSupport(void) { return HIc_Support(m_datas); }

  HIc_Obj getAttrExist(const HIc_String& _attrName)
  {
    STAT_TIC("Mock::HIc_Obj", "getAttrExist")
    STAT("_attrName: " << _attrName)
    STAT("current fullname: " << m_datas.m_fullname)
    std::string attr_fullname;
    if (m_datas.m_fullname == ".")
    {
      attr_fullname = "." + _attrName;
    }
    else
    {
      attr_fullname = m_datas.m_fullname + "." + _attrName;
    }
    STAT("find fullname: " << attr_fullname)
    for (const auto& it : m_datas.m_jdatas.items())
    {
      if (it.key() == attr_fullname)
      {
        STAT_RETURN(HIc_Obj(HIc_Describe(m_datas, attr_fullname)), attr_fullname << " found")
      }
    }
    STAT_RETURN(HIc_Obj(HIc_Describe(m_datas, "")), attr_fullname << " not found")
  }

  HIc_Obj getAttr(const HIc_String& _attrName)
  {
    STAT_TIC("Mock::HIc_Obj", "getAttr")
    STAT("_attrName: " << _attrName)
    return HIc_Obj(HIc_Describe(m_datas, m_datas.m_fullname + "." + _attrName));
  }

  std::vector<HIc_Obj> search(const HIc_String& _typeName)
  {
    STAT_TIC("Mock::HIc_Obj", "search")
    STAT("_typeName: " << _typeName)
    std::vector<HIc_Obj> ret(0, HIc_Describe(m_datas, ""));
    bool isExist = false;
    for (const auto& it : m_datas.m_jdatas[m_datas.m_fullname].items())
    {
      if (it.key() == "@search")
      {
        isExist = true;
        break;
      }
    }
    if (!isExist)
    {
      return ret;
    }
    isExist = false;
    for (const auto& it : m_datas.m_jdatas[m_datas.m_fullname]["@search"].items())
    {
      if (it.key() == _typeName)
      {
        isExist = true;
        break;
      }
    }
    if (!isExist)
    {
      return ret;
    }
    auto maillages = m_datas.m_jdatas[m_datas.m_fullname]["@search"][_typeName];
    for (auto it : maillages)
    {
      ret.emplace_back(HIc_Obj(HIc_Describe(m_datas, it)));
    }
    STAT_RETURN(ret, "")
  }

  void getVal(HIc_String& _val) { _val = m_datas.m_jdatas[m_datas.m_fullname]["@data"]; }
  void getVal(char** _val, std::size_t _nbElement, std::size_t _maxSize) {}
  void getVal(std::size_t& _val) { _val = m_datas.m_jdatas[m_datas.m_fullname]["@data"]; }
  void getVal(unsigned char& _val) { _val = m_datas.m_jdatas[m_datas.m_fullname]["@data"]; }
  void getVal(unsigned int& _val) { _val = m_datas.m_jdatas[m_datas.m_fullname]["@data"]; }
  void getVal(int& _val) { _val = m_datas.m_jdatas[m_datas.m_fullname]["@data"]; }
  void getVal(long& _val)
  {
    std::cerr << "getVal(long&) fullname: " << m_datas.m_fullname << " "
              << m_datas.m_jdatas[m_datas.m_fullname]["@data"] << std::endl;
    _val = m_datas.m_jdatas[m_datas.m_fullname]["@data"];
  }
  void getVal(double& _val) { _val = m_datas.m_jdatas[m_datas.m_fullname]["@data"]; }

  std::size_t getNbVals(void) { return 0; }

  void getVal(std::size_t* _val, std::size_t _nb = 1)
  {
    std::cerr << "getVal(std::size_t*) fullname: " << m_datas.m_fullname << " "
              << m_datas.m_jdatas[m_datas.m_fullname]["@data"] << std::endl;
    if (_nb == 1)
    {
      *_val = m_datas.m_jdatas[m_datas.m_fullname]["@data"];
    }
    else
    {
      std::size_t i = 0;
      for (auto it : m_datas.m_jdatas[m_datas.m_fullname]["@data"])
      {
        _val[i++] = it;
      }
    }
  }
  void getVal(unsigned char* _val, std::size_t _nb = 1)
  {
    std::cerr << "getVal(unsigned char*) fullname: " << m_datas.m_fullname << " "
              << m_datas.m_jdatas[m_datas.m_fullname]["@data"] << std::endl;
    if (_nb == 1)
    {
      *_val = m_datas.m_jdatas[m_datas.m_fullname]["@data"];
    }
    else
    {
      std::size_t i = 0;
      for (auto it : m_datas.m_jdatas[m_datas.m_fullname]["@data"])
      {
        _val[i++] = it;
      }
    }
  }
  void getVal(unsigned int* _val, std::size_t _nb = 1)
  {
    std::cerr << "getVal(unsigned int*) fullname: " << m_datas.m_fullname << " "
              << m_datas.m_jdatas[m_datas.m_fullname]["@data"] << std::endl;
    if (_nb == 1)
    {
      *_val = m_datas.m_jdatas[m_datas.m_fullname]["@data"];
    }
    else
    {
      std::size_t i = 0;
      for (auto it : m_datas.m_jdatas[m_datas.m_fullname]["@data"])
      {
        _val[i++] = it;
      }
    }
  }
  void getVal(int* _val, std::size_t _nb = 1)
  {
    std::cerr << "getVal(int*) fullname: " << m_datas.m_fullname << " "
              << m_datas.m_jdatas[m_datas.m_fullname]["@data"] << std::endl;
    if (_nb = 1)
    {
      *_val = m_datas.m_jdatas[m_datas.m_fullname]["@data"];
    }
    else
    {
      std::size_t i = 0;
      for (auto it : m_datas.m_jdatas[m_datas.m_fullname]["@data"])
      {
        _val[i++] = it;
      }
    }
  }
  void getVal(long* _val, std::size_t _nb = 1)
  {
    std::cerr << "getVal(long*) fullname: " << m_datas.m_fullname << " "
              << m_datas.m_jdatas[m_datas.m_fullname]["@data"] << std::endl;
    if (_nb == 1)
    {
      *_val = m_datas.m_jdatas[m_datas.m_fullname]["@data"];
    }
    else
    {
      std::size_t i = 0;
      for (auto it : m_datas.m_jdatas[m_datas.m_fullname]["@data"])
      {
        _val[i++] = it;
      }
    }
  }
  void getVal(double* _val, std::size_t _nb = 1) {
    std::cerr << "getVal(double) fullname: " << m_datas.m_fullname << " "
              << m_datas.m_jdatas[m_datas.m_fullname]["@data"] << std::endl;
    if (_nb)
    {
      *_val = m_datas.m_jdatas[m_datas.m_fullname]["@data"];
    }
    else
    {
      std::size_t i = 0;
      for (auto it : m_datas.m_jdatas[m_datas.m_fullname]["@data"])
      {
        _val[i++] = it;
      }
    }
  }

  void getAttrVal(HIc_String& _attrName, HIc_String& _val) { getAttr(_attrName).getVal(_val); }

  void getAttrVal(HIc_String _attrName, std::size_t& _val) { getAttr(_attrName).getVal(_val); }
  void getAttrVal(HIc_String _attrName, unsigned char& _val) { getAttr(_attrName).getVal(_val); }
  void getAttrVal(HIc_String _attrName, unsigned int& _val) { getAttr(_attrName).getVal(_val); }
  void getAttrVal(HIc_String _attrName, int& _val) { getAttr(_attrName).getVal(_val); }
  void getAttrVal(HIc_String _attrName, long& _val) { getAttr(_attrName).getVal(_val); }
  void getAttrVal(HIc_String _attrName, double& _val) { getAttr(_attrName).getVal(_val); }
  void getAttrVal(HIc_String _attrName, char** _val, std::size_t _nbElement, std::size_t _maxSize)
  {
    getAttr(_attrName).getVal(_val, _nbElement, _maxSize);
  }
  void getAttrVal(HIc_String _attrName, std::size_t* _val, std::size_t _nb = 1)
  {
    getAttr(_attrName).getVal(_val, _nb);
  }
  void getAttrVal(HIc_String _attrName, unsigned char* _val, std::size_t _nb = 1)
  {
    getAttr(_attrName).getVal(_val, _nb);
  }
  void getAttrVal(HIc_String _attrName, unsigned int* _val, std::size_t _nb = 1)
  {
    getAttr(_attrName).getVal(_val, _nb);
  }
  void getAttrVal(HIc_String _attrName, int* _val, std::size_t _nb = 1)
  {
    getAttr(_attrName).getVal(_val, _nb);
  }
  void getAttrVal(HIc_String _attrName, long* _val, std::size_t _nb = 1)
  {
    getAttr(_attrName).getVal(_val, _nb);
  }
  void getAttrVal(HIc_String _attrName, double* _val, std::size_t _nb = 1)
  {
    getAttr(_attrName).getVal(_val, _nb);
  }
};

class HIc_Ctx : public STAT_CLASS
{
private:
  const HIc_Describe m_datas;
  bool m_is_open;

public:
  HIc_Ctx(const HIc_Describe& datas)
    : m_datas(datas)
    , m_is_open(false)
  {
    STAT_TIC("Mock::HIc_Ctx", "HIc_Ctx")
    STAT_TAC
  }

  virtual ~HIc_Ctx(void)
  {
    STAT_TIC("Mock::HIc_Ctx", "~HIc_Ctx")
    STAT_TAC
  }

  void open(void) { m_is_open = true; }
  bool isOpen(void) { return m_is_open; }

  HIc_Obj getRoot(void){ STAT_TIC("Mock::HIc_Ctx", "getRoot") STAT_ASSERT(isOpen(),
    "Call open() on HIc_Ctx before") STAT_RETURN(HIc_Obj(HIc_Describe(m_datas, ".")), "") }

  std::vector<HIc_Obj> search(const HIc_String& _typeName)
  {
    STAT_TIC("Mock::HIc_Ctx", "search")
    STAT("_typeName: " << _typeName)
    STAT_RETURN(getRoot().search(_typeName), "")
  }

  void close(void) { m_is_open = false; }
};

class HIc_Base : public STAT_CLASS
{
private:
  bool m_is_open;
  json m_jdatas;
  std::map<std::string, std::string> m_conf;

public:
  HIc_Base(const HIc_String& _name = HIc_String(""), const HIc_String& _confName = HIc_String(""))
    : m_is_open(false)
  {
    STAT_TIC("Mock::HIc_Base", "HIc_Base")
    STAT("_name: " << _name << " _confName:" << _confName)
    STAT("_name and _confName ignored in this mock")
    STAT_TAC
  }

  virtual ~HIc_Base(void)
  {
    STAT_TIC("Mock::HIc_Base", "~HIc_Base")
    STAT_TAC
  }

  void setItemConf(const HIc_String& _item, const HIc_String& _val)
  {
    STAT_TIC("Mock::HIc_Base", "setItemConf")
    STAT("_item: " << _item << " _val: " << _val)
    m_conf[_item] = _val;
    STAT_TAC
  }

  void setMode(const HIc_String& _val) { this->setItemConf("mode", _val); }

  void open(void)
  {
    STAT_TIC("Mock::HIc_Base", "open")
    std::string filename = m_conf["read_dir"] + "/" + m_conf["bd_name"] + "-.json";
    STAT("filename: " << filename)
    std::ifstream base(filename, std::ios::in);
    if (base)
    {
      base >> m_jdatas;
      base.close();
      STAT(filename << " is loaded")
      m_is_open = true;
    } else {
      STAT_WARNING(filename << " is NOT loaded !")
    }
    STAT_TAC
  }

  bool isOpen(void) { return m_is_open; }

  bool isCstNbDomains(void) { return true; }

  long getNbDomains(void)
  {
    STAT_TIC("_Base", "getNbDomains")
    STAT_ASSERT(isOpen(), "Call open() on HIc_Base before")
    long nbDomains = m_jdatas["@domains"].size();
    STAT_RETURN(nbDomains, nbDomains)
  }
  long getNbDomains(std::size_t _iTemps)
  {
    STAT_TIC("_Base", "getNbDomains")
    STAT("_iTemps: " << _iTemps)
    STAT("In this mock, we consider that the number of domains does not vary according to the "
         "simulation time.")
    STAT_RETURN(getNbDomains(), "")
  }

  void getTimeList(std::vector<double>& _times)
  {
    STAT_TIC("_Base", "getTimeList")
    STAT_ASSERT(isOpen(), "Call open() on HIc_Base before")
    std::cerr << m_jdatas["@times"] << std::endl;
    for (auto it : m_jdatas["@times"])
    {
      _times.emplace_back(it);
    }
    STAT("times.size(): " << _times.size())
    STAT_TAC
  };

  HIc_Ctx getCtxPar(double _time,
    int _numDomain,
    const HIc_String& _name = HIc_String(""),
    const HIc_String& _confName = HIc_String(""))
  {
    STAT_TIC("HIc_Base", "createBase")
    STAT_ASSERT(isOpen(), "Call open() on HIc_Base before")
    STAT("_time: " << std::to_string(_time) << " _numDomain:" << std::to_string(_numDomain))
    STAT("_name: " << _name << " _confName:" << _confName)
    HIc_Describe datas(m_jdatas);
    datas.m_time = _time;
    datas.m_numDomain = _numDomain;
    STAT_RETURN(HIc_Ctx(datas), "")
  }

  void close(void)
  {
    m_is_open = false;
    m_jdatas = json();
  }
};

class HIc_Api : public STAT_CLASS
{
public:
  HIc_Api(void){ STAT_TIC("Mock::HIc_Api", "HIc_Api") STAT("Mode mock") STAT_TAC }

  HIc_Api(const std::string& _name, const HIc_String& _confName = "")
  {
    STAT_TIC("Mock::HIc_Api", "HIc_Api")
    STAT("Mode mock with _name: " << _name << " _confName: " << _confName)
    STAT_TAC
  }

  virtual ~HIc_Api(void)
  {
    STAT_TIC("Mock::HIc_Api", "~HIc_Api")
    STAT_TAC
  }

  bool getBaseNameFromFileName(const HIc_String& _fileName, HIc_String& _bdName)
  {
    STAT_TIC("HIc_Api", "getBaseNameFromFileName")
    STAT("Original _fileName: " << _fileName)
    std::string _baseName = _fileName;
    size_t slash = _baseName.find_last_of('/');
    if (slash != std::string::npos)
    {
      _baseName = _baseName.substr(slash + 1);
    }
    STAT("After slash _baseName: " << _baseName)
    // Suppress extension ".a-b"
    size_t point = _baseName.find_last_of('.');
    if (point == std::string::npos)
    {
      _baseName = "";
      STAT_RETURN(false, "return false: point treatment _baseName: " << _baseName)
    }
    _baseName = _baseName.substr(0, point);
    STAT("Before point _baseName: " << _baseName)
    // Save before '-'
    size_t hyphen = _baseName.find_last_of('-');
    if (hyphen == std::string::npos)
    {
      _baseName = "";
      STAT_RETURN(false, "return false: hyphen treatment _baseName: " << _baseName)
    }
    _baseName = _baseName.substr(0, hyphen);
    STAT("Before hyphen _baseName: " << _baseName)
    STAT("Return _bdName:" << _bdName)
    _bdName = _baseName;
    STAT_RETURN(true, "return true before hyphen _bdName: " << _bdName)
  }

  HIc_Base createBase(const HIc_String& _name = HIc_String(""),
    const HIc_String& _confName = HIc_String(""))
  {
    STAT_TIC("HIc_Api", "createBase")
    STAT_RETURN(HIc_Base(_name, _confName), "")
  }
};

void HIc_Init_Standard_Services(const HIc_Api& _api);
void HIc_Init_Standard_Site(const HIc_Api& _api, const HIc_String& _confName);

typedef HIc_Api Api;
}

#define HIC_USE using namespace Mock_HIc;
#define HERCULE Mock_HIc
#define HERCULE_NAME Mock_HIc

#endif // _MOCKHIC_HPP_
