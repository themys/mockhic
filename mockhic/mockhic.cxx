#include <iostream>
#include <utility>

#include "mockhic_macro.hpp"

#include "mockhic.hpp"
HIC_USE

#include "json.hpp"
using json = nlohmann::json;

//- HIc_P_Describe ---------------------------------------------------------------------------------
class HIc_P_Describe
{
public:
  const std::shared_ptr<json> m_jdatas;
  int m_time;
  int m_numDomain;
  std::string m_fullname;
  std::size_t m_i_support_desc;

  explicit HIc_P_Describe(std::shared_ptr<json> _jdatas)
    : m_jdatas(std::move(_jdatas))
    , m_time(0)
    , m_numDomain(0)
    , m_fullname("")
    , m_i_support_desc(0)
  {
  }

  explicit HIc_P_Describe(const std::shared_ptr<HIc_P_Describe>& _datas)
    : m_jdatas(_datas->m_jdatas)
    , m_time(_datas->m_time)
    , m_numDomain(_datas->m_numDomain)
    , m_fullname(_datas->m_fullname)
    , m_i_support_desc(_datas->m_i_support_desc)
  {
  }
  explicit HIc_P_Describe(const std::shared_ptr<HIc_P_Describe>& _datas,
    const std::string& _fullname)
    : m_jdatas(_datas->m_jdatas)
    , m_time(_datas->m_time)
    , m_numDomain(_datas->m_numDomain)
    , m_fullname(std::move(_fullname))
    , m_i_support_desc(0)
  {
  }
  explicit HIc_P_Describe(const std::shared_ptr<HIc_P_Describe>& _datas, int _time, int _numDomain)
    : m_jdatas(_datas->m_jdatas)
    , m_time(_time)
    , m_numDomain(_numDomain)
    , m_fullname("")
    , m_i_support_desc(0)
  {
  }
  [[nodiscard]] const json& getJson() const { return *m_jdatas; }
};

//- HIc_SupportDesc --------------------------------------------------------------------------------

bool Mockhic::HIc_SupportDesc::isASel(void) const
{
  for (const auto& it :
    m_datas->getJson()[m_datas->m_fullname]["@support"][m_datas->m_i_support_desc].items())
  {
    if (it.key() == "@sel")
    {
      return true;
    }
  }
  return false;
}
std::size_t Mockhic::HIc_SupportDesc::getNbSel(void) const
{
  for (const auto& it :
    m_datas->getJson()[m_datas->m_fullname]["@support"][m_datas->m_i_support_desc].items())
  {
    if (it.key() == "@sel")
    {
      return m_datas->getJson()[m_datas->m_fullname]["@support"][m_datas->m_i_support_desc]["@sel"]
        .size();
    }
  }
  return -1;
}
const long* Mockhic::HIc_SupportDesc::getSel(void)
{
  for (const auto& it :
    m_datas->getJson()[m_datas->m_fullname]["@support"][m_datas->m_i_support_desc].items())
  {
    if (it.key() == "@sel")
    {
      m_sel.resize(getNbSel());
      std::size_t i = 0;
      for (auto it :
        m_datas->getJson()[m_datas->m_fullname]["@support"][m_datas->m_i_support_desc]["@sel"])
      {
        m_sel[i++] = it;
      }
      return m_sel.data();
    }
  }
  return nullptr;
}

//- HIc_Support ------------------------------------------------------------------------------------

bool Mockhic::HIc_Support::isNull()
{
  for (const auto& it : m_datas->getJson()[m_datas->m_fullname].items())
  {
    if (it.key() == "@support")
    {
      return false;
    }
  }
  return true;
}

std::size_t Mockhic::HIc_Support::getNbSupportDesc(void)
{
  return m_datas->getJson()[m_datas->m_fullname]["@support"].size();
}

HIc_SupportDesc Mockhic::HIc_Support::getSupportDesc(std::size_t _i)
{
  std::shared_ptr<HIc_P_Describe> datas = std::make_shared<HIc_P_Describe>(m_datas);
  datas->m_i_support_desc = _i;
  return HIc_SupportDesc(datas);
}

//- HIc_Obj ----------------------------------------------------------------------------------------

bool Mockhic::HIc_Obj::isNull()
{
  return m_datas == nullptr || m_datas->m_fullname == "";
}

const HIc_String Mockhic::HIc_Obj::getName(void)
{
  if (m_datas->m_fullname == ".")
  {
    return ".";
  }
  return m_datas->m_fullname.substr(m_datas->m_fullname.find_last_of('.') + 1);
}

const HIc_String Mockhic::HIc_Obj::getFullName(void)
{
  return m_datas->m_fullname;
}

const HIc_String Mockhic::HIc_Obj::getTypeName(void)
{
  return m_datas->getJson()[m_datas->m_fullname]["@type"];
}

HIc_Obj Mockhic::HIc_Obj::getAttrIsExist(const HIc_String& _attrName)
{
  MOCKHIC_STAT_TIC("Mock::HIc_Obj", "getAttrIsExist")
  MOCKHIC_STAT("_attrName: " << _attrName)
  MOCKHIC_STAT("current fullname: " << m_datas->m_fullname)
  std::string attr_fullname;
  if (m_datas->m_fullname == ".")
  {
    attr_fullname = "." + _attrName;
  }
  else
  {
    attr_fullname = m_datas->m_fullname + "." + _attrName;
  }
  MOCKHIC_STAT("find fullname: " << attr_fullname)
  json& jtmp = *(m_datas->m_jdatas);
  for (const auto& it : jtmp.items())
  {
    if (it.key() == attr_fullname)
    {
      MOCKHIC_STAT_RETURN(HIc_Obj(std::make_shared<HIc_P_Describe>(m_datas, attr_fullname)),
        attr_fullname << " found")
    }
  }
  MOCKHIC_STAT_RETURN(
    HIc_Obj(std::make_shared<HIc_P_Describe>(m_datas, "")), attr_fullname << " not found")
}

HIc_Obj Mockhic::HIc_Obj::getAttr(const HIc_String& _attrName)
{
  MOCKHIC_STAT_TIC("Mock::HIc_Obj", "getAttr")
  MOCKHIC_STAT("_attrName: " << _attrName)
  std::string fullname;
  if (m_datas->m_fullname == ".")
  {
    fullname = "." + _attrName;
  }
  else
  {
    fullname = m_datas->m_fullname + "." + _attrName;
  }
  return HIc_Obj(std::make_shared<HIc_P_Describe>(m_datas, fullname));
}

std::vector<HIc_Obj> Mockhic::HIc_Obj::search(const HIc_String& _typeName)
{
  MOCKHIC_STAT_TIC("Mock::HIc_Obj", "search")
  MOCKHIC_STAT("_typeName: " << _typeName)
  std::vector<HIc_Obj> ret(0, HIc_Obj(std::make_shared<HIc_P_Describe>(m_datas, "")));
  bool isExist = false;
  for (const auto& it : m_datas->getJson()[m_datas->m_fullname].items())
  {
    if (it.key() == "@search")
    {
      isExist = true;
      break;
    }
  }
  if (!isExist)
  {
    return ret;
  }
  isExist = false;
  for (const auto& it : m_datas->getJson()[m_datas->m_fullname]["@search"].items())
  {
    if (it.key() == _typeName)
    {
      isExist = true;
      break;
    }
  }
  if (!isExist)
  {
    return ret;
  }
  auto maillages = m_datas->getJson()[m_datas->m_fullname]["@search"][_typeName];
  for (auto it : maillages)
  {
    ret.emplace_back(HIc_Obj(std::make_shared<HIc_P_Describe>(m_datas, it)));
    MOCKHIC_STAT("OUTPUT " << it)
  }
  MOCKHIC_STAT_RETURN(ret, "")
}

std::size_t Mockhic::HIc_Obj::getNbVals()
{
  if (m_datas->m_fullname.substr(m_datas->m_fullname.length() - 4) == ".val")
  {
    return m_datas->getJson()[m_datas->m_fullname]["@data"].size();
  }
  return m_datas->getJson()[m_datas->m_fullname + ".val"]["@data"].size();
}

void Mockhic::HIc_Obj::getVal(HIc_String& _val)
{
  _val = m_datas->getJson()[m_datas->m_fullname]["@data"];
}

void Mockhic::HIc_Obj::getVal(char** _val, std::size_t _nbElement, std::size_t _maxSize) {}

void Mockhic::HIc_Obj::getVal(std::size_t& _val)
{
  _val = m_datas->getJson()[m_datas->m_fullname]["@data"];
}

void Mockhic::HIc_Obj::getVal(unsigned char& _val)
{
  _val = m_datas->getJson()[m_datas->m_fullname]["@data"];
}

void Mockhic::HIc_Obj::getVal(unsigned int& _val)
{
  _val = m_datas->getJson()[m_datas->m_fullname]["@data"];
}

void Mockhic::HIc_Obj::getVal(int& _val)
{
  _val = m_datas->getJson()[m_datas->m_fullname]["@data"];
}

void Mockhic::HIc_Obj::getVal(long& _val)
{
  std::cerr << "getVal(long&) fullname: " << m_datas->m_fullname << " "
            << m_datas->getJson()[m_datas->m_fullname]["@data"] << std::endl;
  _val = m_datas->getJson()[m_datas->m_fullname]["@data"];
}

void Mockhic::HIc_Obj::getVal(float& _val)
{
  _val = m_datas->getJson()[m_datas->m_fullname]["@data"];
}

void Mockhic::HIc_Obj::getVal(double& _val)
{
  _val = m_datas->getJson()[m_datas->m_fullname]["@data"];
}

void Mockhic::HIc_Obj::getVal(std::size_t* _val, std::size_t _nb)
{
  std::cerr << "getVal(std::size_t*) fullname: " << m_datas->m_fullname << " "
            << m_datas->getJson()[m_datas->m_fullname]["@data"] << std::endl;
  if (_nb == 1)
  {
    *_val = m_datas->getJson()[m_datas->m_fullname]["@data"];
  }
  else
  {
    std::size_t i = 0;
    for (auto it : m_datas->getJson()[m_datas->m_fullname]["@data"])
    {
      _val[i++] = it;
    }
  }
}

void Mockhic::HIc_Obj::getVal(unsigned char* _val, std::size_t _nb)
{
  std::cerr << "getVal(unsigned char*) fullname: " << m_datas->m_fullname << " "
            << m_datas->getJson()[m_datas->m_fullname]["@data"] << std::endl;
  if (_nb == 1)
  {
    *_val = m_datas->getJson()[m_datas->m_fullname]["@data"];
  }
  else
  {
    std::size_t i = 0;
    for (auto it : m_datas->getJson()[m_datas->m_fullname]["@data"])
    {
      _val[i++] = it;
    }
  }
}

void Mockhic::HIc_Obj::getVal(unsigned int* _val, std::size_t _nb)
{
  std::cerr << "getVal(unsigned int*) fullname: " << m_datas->m_fullname << " "
            << m_datas->getJson()[m_datas->m_fullname]["@data"] << std::endl;
  if (_nb == 1)
  {
    *_val = m_datas->getJson()[m_datas->m_fullname]["@data"];
  }
  else
  {
    std::size_t i = 0;
    for (auto it : m_datas->getJson()[m_datas->m_fullname]["@data"])
    {
      _val[i++] = it;
    }
  }
}

void Mockhic::HIc_Obj::getVal(int* _val, std::size_t _nb)
{
  std::cerr << "getVal(int*) fullname: " << m_datas->m_fullname << " "
            << m_datas->getJson()[m_datas->m_fullname]["@data"] << std::endl;
  if (_nb == 1)
  {
    *_val = m_datas->getJson()[m_datas->m_fullname]["@data"];
  }
  else
  {
    std::size_t i = 0;
    for (auto it : m_datas->getJson()[m_datas->m_fullname]["@data"])
    {
      _val[i++] = it;
    }
  }
}

void Mockhic::HIc_Obj::getVal(long* _val, std::size_t _nb)
{
  std::cerr << "getVal(long*) fullname: " << m_datas->m_fullname << " "
            << m_datas->getJson()[m_datas->m_fullname]["@data"] << std::endl;
  if (_nb == 1)
  {
    *_val = m_datas->getJson()[m_datas->m_fullname]["@data"];
  }
  else
  {
    std::size_t i = 0;
    for (auto it : m_datas->getJson()[m_datas->m_fullname]["@data"])
    {
      _val[i++] = it;
    }
  }
}

void Mockhic::HIc_Obj::getVal(float* _val, std::size_t _nb)
{
  std::cerr << "getVal(double) fullname: " << m_datas->m_fullname << " "
            << m_datas->getJson()[m_datas->m_fullname]["@data"] << std::endl;
  if (_nb == 1)
  {
    *_val = m_datas->getJson()[m_datas->m_fullname]["@data"];
  }
  else
  {
    std::size_t i = 0;
    for (auto it : m_datas->getJson()[m_datas->m_fullname]["@data"])
    {
      _val[i++] = it;
    }
  }
}

void Mockhic::HIc_Obj::getVal(double* _val, std::size_t _nb)
{
  std::cerr << "getVal(double) fullname: " << m_datas->m_fullname << " "
            << m_datas->getJson()[m_datas->m_fullname]["@data"] << std::endl;
  if (_nb == 1)
  {
    *_val = m_datas->getJson()[m_datas->m_fullname]["@data"];
  }
  else
  {
    std::size_t i = 0;
    for (auto it : m_datas->getJson()[m_datas->m_fullname]["@data"])
    {
      _val[i++] = it;
    }
  }
}

//- HIc_Ctx ----------------------------------------------------------------------------------------

HIc_Obj Mockhic::HIc_Ctx::getRoot(void)
{
  MOCKHIC_STAT_TIC("Mock::HIc_Ctx", "getRoot");
  MOCKHIC_STAT_ASSERT(isOpen(), "Call open() on HIc_Ctx before");
  MOCKHIC_STAT_RETURN(HIc_Obj(std::make_shared<HIc_P_Describe>(m_datas, ".")), "");
}

//- HIc_Base ---------------------------------------------------------------------------------------

void Mockhic::HIc_Base::open(void)
{
  MOCKHIC_STAT_TIC("Mock::HIc_Base", "open");
  std::string filename = m_conf["read_dir"] + "/" + m_conf["bd_name"] + "-.json";
  MOCKHIC_STAT("filename: " << filename)
  json jdatas;
  std::ifstream base(filename, std::ios::in);
  if (base)
  {
    base >> jdatas;
    base.close();
    MOCKHIC_STAT(filename << " is loaded")
    m_datas = std::make_shared<HIc_P_Describe>(std::make_shared<json>(jdatas));
    m_is_open = true;
  }
  else
  {
    MOCKHIC_STAT_WARNING(filename << " is NOT loaded !")
  }
  MOCKHIC_STAT_TAC
}

long Mockhic::HIc_Base::getNbDomains(void)
{
  MOCKHIC_STAT_TIC("_Base", "getNbDomains")
  MOCKHIC_STAT_ASSERT(isOpen(), "Call open() on HIc_Base before")
  long nbDomains = m_datas->getJson()["@domains"].size();
  MOCKHIC_STAT_RETURN(nbDomains, nbDomains)
}

void Mockhic::HIc_Base::getTimeList(std::vector<double>& _times)
{
  MOCKHIC_STAT_TIC("_Base", "getTimeList")
  MOCKHIC_STAT_ASSERT(isOpen(), "Call open() on HIc_Base before")
  std::cerr << m_datas->getJson()["@times"] << std::endl;
  for (auto it : m_datas->getJson()["@times"])
  {
    _times.emplace_back(it);
  }
  MOCKHIC_STAT("times.size(): " << _times.size())
  MOCKHIC_STAT_TAC
}

HIc_Ctx Mockhic::HIc_Base::getCtxPar(double _time,
  int _numDomain,
  const HIc_String& _name,
  const HIc_String& _confName)
{
  MOCKHIC_STAT_TIC("HIc_Base", "createBase")
  MOCKHIC_STAT_ASSERT(isOpen(), "Call open() on HIc_Base before")
  MOCKHIC_STAT("_time: " << std::to_string(_time) << " _numDomain:" << std::to_string(_numDomain))
  MOCKHIC_STAT("_name: " << _name << " _confName:" << _confName)
  MOCKHIC_STAT_RETURN(HIc_Ctx(std::make_shared<HIc_P_Describe>(m_datas, _time, _numDomain)), "")
}

//--------------------------------------------------------------------------------------------------

void HIc_Dictionary::getDerivedTypeList(const std::string& _typeName,
  std::vector<std::string>& _vectorTypeName,
  bool _recursive) {
  MOCKHIC_STAT_TIC_MET("HIc_Dictionary", "getDerivedTypeList", "")
  MOCKHIC_STAT_ASSERT(_recursive, "Not implemented _recursive=false");
  _vectorTypeName = this->m_derivedTypeList[_typeName];
  MOCKHIC_STAT_TAC
}

//--------------------------------------------------------------------------------------------------

void Mockhic::HIc_Init_Standard_Services(const HIc_Api& _api)
{
  MOCKHIC_STAT_TIC_MET("Mock", "HIc_Init_Standard_Services", "")
  MOCKHIC_STAT_TAC
}

void Mockhic::HIc_Init_Standard_Site(const HIc_Api& _api, const HIc_String& _confName)
{
  MOCKHIC_STAT_TIC_MET("Mock", "HIc_Init_Standard_Site", "")
  MOCKHIC_STAT("_confName:" << _confName)
  MOCKHIC_STAT_TAC
}