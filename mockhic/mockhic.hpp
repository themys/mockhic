#ifndef _MOCKHIC_HPP_
#define _MOCKHIC_HPP_

#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "mockhic_macro.hpp"

//- HIc_P_Describe ---------------------------------------------------------------------------------

class HIc_P_Describe;

namespace Mockhic
{
typedef std::exception Exception;
typedef std::string HIc_String;
typedef HIc_String HERString;

class HIc_SupportDesc;
class HIc_Support;
class HIc_Obj;
class HIc_Ctx;
class HIc_Base;
class HIc_Api;

//- HIc_SupportDesc --------------------------------------------------------------------------------

class HIc_SupportDesc : public MOCKHIC_STAT_CLASS
{
private:
  std::shared_ptr<HIc_P_Describe> m_datas;
  std::vector<long> m_sel;

public:
  HIc_SupportDesc() {}
  HIc_SupportDesc(std::shared_ptr<HIc_P_Describe> datas)
    : m_datas(datas)
  {
  }
  virtual ~HIc_SupportDesc(){};

  bool isNull() { return m_datas != nullptr; }

  bool isASel(void) const;
  std::size_t getNbSel(void) const;
  const long* getSel(void);
};

//- HIc_Support ------------------------------------------------------------------------------------

class HIc_Support : public MOCKHIC_STAT_CLASS
{
private:
  std::shared_ptr<HIc_P_Describe> m_datas;

public:
  HIc_Support(std::shared_ptr<HIc_P_Describe> datas)
    : m_datas(datas)
  {
  }
  virtual ~HIc_Support() {}

  bool isNull();

  std::size_t getNbSupportDesc(void);
  HIc_SupportDesc getSupportDesc(std::size_t _i);
};

//- HIc_Obj ----------------------------------------------------------------------------------------

class HIc_Obj : public MOCKHIC_STAT_CLASS
{
private:
  std::shared_ptr<HIc_P_Describe> m_datas;

public:
  HIc_Obj()
    : m_datas(nullptr)
  {
  }
  HIc_Obj(std::shared_ptr<HIc_P_Describe> _datas)
    : m_datas(_datas)
  {
  }
  virtual ~HIc_Obj(void) {}

  bool isNull();

  const HIc_String getName(void);
  const HIc_String getTypeName(void);
  const HIc_String getFullName(void);

  HIc_Support getSupport(void) { return HIc_Support(m_datas); }

  HIc_Obj getAttrIsExist(const HIc_String& _attrName);
  HIc_Obj getAttr(const HIc_String& _attrName);

  std::vector<HIc_Obj> search(const HIc_String& _typeName);

  std::size_t getNbVals(void);

  void getVal(HIc_String& _val);
  void getVal(char** _val, std::size_t _nbElement, std::size_t _maxSize);
  void getVal(std::size_t& _val);
  void getVal(unsigned char& _val);
  void getVal(unsigned int& _val);
  void getVal(int& _val);
  void getVal(long& _val);
  void getVal(float& _val);
  void getVal(double& _val);

  void getVal(std::size_t* _val, std::size_t _nb = 1);
  void getVal(unsigned char* _val, std::size_t _nb = 1);
  void getVal(unsigned int* _val, std::size_t _nb = 1);
  void getVal(int* _val, std::size_t _nb = 1);
  void getVal(long* _val, std::size_t _nb = 1);
  void getVal(float* _val, std::size_t _nb = 1);
  void getVal(double* _val, std::size_t _nb = 1);

  void getAttrVal(HIc_String& _attrName, HIc_String& _val) { getAttr(_attrName).getVal(_val); }

  void getAttrVal(HIc_String _attrName, std::size_t& _val) { getAttr(_attrName).getVal(_val); }
  void getAttrVal(HIc_String _attrName, unsigned char& _val) { getAttr(_attrName).getVal(_val); }
  void getAttrVal(HIc_String _attrName, unsigned int& _val) { getAttr(_attrName).getVal(_val); }
  void getAttrVal(HIc_String _attrName, int& _val) { getAttr(_attrName).getVal(_val); }
  void getAttrVal(HIc_String _attrName, long& _val) { getAttr(_attrName).getVal(_val); }
  void getAttrVal(HIc_String _attrName, float& _val) { getAttr(_attrName).getVal(_val); }
  void getAttrVal(HIc_String _attrName, double& _val) { getAttr(_attrName).getVal(_val); }
  void getAttrVal(HIc_String _attrName, char** _val, std::size_t _nbElement, std::size_t _maxSize)
  {
    getAttr(_attrName).getVal(_val, _nbElement, _maxSize);
  }
  void getAttrVal(HIc_String _attrName, std::size_t* _val, std::size_t _nb = 1)
  {
    getAttr(_attrName).getVal(_val, _nb);
  }
  void getAttrVal(HIc_String _attrName, unsigned char* _val, std::size_t _nb = 1)
  {
    getAttr(_attrName).getVal(_val, _nb);
  }
  void getAttrVal(HIc_String _attrName, unsigned int* _val, std::size_t _nb = 1)
  {
    getAttr(_attrName).getVal(_val, _nb);
  }
  void getAttrVal(HIc_String _attrName, int* _val, std::size_t _nb = 1)
  {
    getAttr(_attrName).getVal(_val, _nb);
  }
  void getAttrVal(HIc_String _attrName, long* _val, std::size_t _nb = 1)
  {
    getAttr(_attrName).getVal(_val, _nb);
  }
  void getAttrVal(HIc_String _attrName, float* _val, std::size_t _nb = 1)
  {
    getAttr(_attrName).getVal(_val, _nb);
  }
  void getAttrVal(HIc_String _attrName, double* _val, std::size_t _nb = 1)
  {
    getAttr(_attrName).getVal(_val, _nb);
  }
};

//- HIc_Ctx ----------------------------------------------------------------------------------------

class HIc_Ctx : public MOCKHIC_STAT_CLASS
{
private:
  std::shared_ptr<HIc_P_Describe> m_datas;
  bool m_is_open;

public:
  HIc_Ctx()
    : m_is_open(false){ MOCKHIC_STAT_TIC("Mock::HIc_Ctx", "HIc_Ctx") MOCKHIC_STAT_TAC }

    HIc_Ctx(std::shared_ptr<HIc_P_Describe> datas)
    : m_datas(datas)
    , m_is_open(false)
  {
    MOCKHIC_STAT_TIC("Mock::HIc_Ctx", "HIc_Ctx")
    MOCKHIC_STAT_TAC
  }

  virtual ~HIc_Ctx(void)
  {
    MOCKHIC_STAT_TIC("Mock::HIc_Ctx", "~HIc_Ctx")
    MOCKHIC_STAT_TAC
  }

  bool isNull(void) { return false; }

  void open(void) { m_is_open = true; }
  bool isOpen(void) { return m_is_open; }

  HIc_Obj getRoot(void);

  std::vector<HIc_Obj> search(const HIc_String& _typeName)
  {
    MOCKHIC_STAT_TIC("Mock::HIc_Ctx", "search")
    MOCKHIC_STAT("_typeName: " << _typeName)
    MOCKHIC_STAT_RETURN(getRoot().search(_typeName), "")
  }

  void close(void) { m_is_open = false; }
};

//- HIc_Base ---------------------------------------------------------------------------------------

class HIc_Base : public MOCKHIC_STAT_CLASS
{
private:
  std::shared_ptr<HIc_P_Describe> m_datas;
  bool m_is_open;
  std::map<std::string, std::string> m_conf;

public:
  HIc_Base(const HIc_String& _name = HIc_String(""), const HIc_String& _confName = HIc_String(""))
    : m_is_open(false)
  {
    MOCKHIC_STAT_TIC("Mock::HIc_Base", "HIc_Base")
    MOCKHIC_STAT("_name: " << _name << " _confName:" << _confName)
    MOCKHIC_STAT("_name and _confName ignored in this mock")
    MOCKHIC_STAT_TAC
  }

  virtual ~HIc_Base(void)
  {
    MOCKHIC_STAT_TIC("Mock::HIc_Base", "~HIc_Base")
    MOCKHIC_STAT_TAC
  }

  void setItemConf(const HIc_String& _item, const HIc_String& _val)
  {
    MOCKHIC_STAT_TIC("Mock::HIc_Base", "setItemConf")
    MOCKHIC_STAT("_item: " << _item << " _val: " << _val)
    m_conf[_item] = _val;
    MOCKHIC_STAT_TAC
  }

  void setMode(const HIc_String& _val) { this->setItemConf("mode", _val); }

  bool isNull(void) { return false; }

  void open(void);
  bool isOpen(void) { return m_is_open; }

  bool isCstNbDomains(void) { return true; }

  long getNbDomains(void);
  long getNbDomains(std::size_t _iTemps)
  {
    MOCKHIC_STAT_TIC("_Base", "getNbDomains")
    MOCKHIC_STAT("_iTemps: " << _iTemps)
    MOCKHIC_STAT(
      "In this mock, we consider that the number of domains does not vary according to the "
      "simulation time.")
    MOCKHIC_STAT_RETURN(getNbDomains(), "")
  }

  void getTimeList(std::vector<double>& _times);

  HIc_Ctx getCtxPar(double _time,
    int _numDomain,
    const HIc_String& _name = HIc_String(""),
    const HIc_String& _confName = HIc_String(""));

  void close(void)
  {
    m_is_open = false;
    m_datas = nullptr;
  }
};

//- HIc_Dictionary ---------------------------------------------------------------------------------

class HIc_Dictionary : public MOCKHIC_STAT_CLASS
{
private:
  std::map<std::string, std::vector<std::string> > m_derivedTypeList = {
    { "GrandeurScalaireEntier", { "GrandeurScalaire", "Grandeur" } },
    { "GrandeurScalaireFlottant", { "GrandeurScalaire", "Grandeur" } },
    // { "GrandeurSpatialeEntier", { "GrandeurSpatiale", "Grandeur"}},
    { "GrandeurSpatialeFlottant", { "GrandeurSpatiale", "Grandeur" } },
    { "Maillage-2D-AMR-S-Ortho-Regulier", { "Maillage-2D-AMR", "Maillage-AMR" } },
    { "Ramses2D",
      { "Maillage-2D-AMR-S-Ortho-Regulier", "Maillage-2D-AMR", "Maillage-AMR", "Maillage" } },
    { "Maillage-3D-AMR-S-Ortho-Regulier", { "Maillage-3D-AMR", "Maillage-AMR", "Maillage" } },
    { "Ramses3D",
      { "Maillage-3D-AMR-S-Ortho-Regulier",
        "Maillage"
        "Maillage-3D-AMR",
        "Maillage-AMR",
        "Maillage" } },
    { "Maillage-0D-NS", { "Maillage" } },
    { "Maillage-1D-NS", { "Maillage" } },
    { "Maillage-2D-NS", { "Maillage" } },
    { "Maillage-3D-NS", { "Maillage" } },
    { "Maillage-0D-S", { "Maillage" } },
    { "Maillage-1D-S", { "Maillage" } },
    { "Maillage-2D-S", { "Maillage" } },
    { "Maillage-3D-S", { "Maillage" } },
    { "Maillage-0D-S-Ortho-Regulier", { "Maillage-0D-S", "Maillage" } },
    { "Maillage-1D-S-Ortho-Regulier", { "Maillage-1D-S", "Maillage" } },
    { "Maillage-2D-S-Ortho-Regulier", { "Maillage-2D-S", "Maillage" } },
    { "Maillage-3D-S-Ortho-Regulier", { "Maillage-3D-S", "Maillage" } }
  };

public:
  HIc_Dictionary(void)
  {
    MOCKHIC_STAT_TIC("Mock::HIc_Dictionary", "HIc_Dictionary")
    MOCKHIC_STAT("Mode mock") MOCKHIC_STAT_TAC
  }

  virtual ~HIc_Dictionary(void)
  {
    MOCKHIC_STAT_TIC("Mock::HIc_Dictionary", "~HIc_Dictionary")
    MOCKHIC_STAT_TAC
  }

  void getDerivedTypeList(const std::string& _typeName,
    std::vector<std::string>& _vectorTypeName,
    bool _recursive = false);
};

//- HIc_Api ----------------------------------------------------------------------------------------

class HIc_Api : public MOCKHIC_STAT_CLASS
{
private:
  std::shared_ptr<HIc_Dictionary> m_dico;

public:
  HIc_Api(
    void){ MOCKHIC_STAT_TIC("Mock::HIc_Api", "HIc_Api") MOCKHIC_STAT("Mode mock") MOCKHIC_STAT_TAC }

  HIc_Api(const std::string& _name, const HIc_String& _confName = "")
  {
    MOCKHIC_STAT_TIC("Mock::HIc_Api", "HIc_Api")
    MOCKHIC_STAT("Mode mock with _name: " << _name << " _confName: " << _confName)
    MOCKHIC_STAT_TAC
  }

  virtual ~HIc_Api(void){ MOCKHIC_STAT_TIC("Mock::HIc_Api", "~HIc_Api") MOCKHIC_STAT_TAC }

  std::shared_ptr<HIc_Dictionary> getCurrentDictionary()
  {
    if (m_dico)
    {
      this->m_dico = std::make_shared<HIc_Dictionary>();
    }
    return this->m_dico;
  }

  bool getBaseNameFromFileName(const HIc_String& _fileName, HIc_String& _bdName)
  {
    MOCKHIC_STAT_TIC("HIc_Api", "getBaseNameFromFileName")
    MOCKHIC_STAT("Original _fileName: " << _fileName)
    std::string _baseName = _fileName;
    size_t slash = _baseName.find_last_of('/');
    if (slash != std::string::npos)
    {
      _baseName = _baseName.substr(slash + 1);
    }
    MOCKHIC_STAT("After slash _baseName: " << _baseName)
    // Suppress extension ".a-b"
    size_t point = _baseName.find_last_of('.');
    if (point == std::string::npos)
    {
      _baseName = "";
      MOCKHIC_STAT_RETURN(false, "return false: point treatment _baseName: " << _baseName)
    }
    _baseName = _baseName.substr(0, point);
    MOCKHIC_STAT("Before point _baseName: " << _baseName)
    // Save before '-'
    size_t hyphen = _baseName.find_last_of('-');
    if (hyphen == std::string::npos)
    {
      _baseName = "";
      MOCKHIC_STAT_RETURN(false, "return false: hyphen treatment _baseName: " << _baseName)
    }
    _baseName = _baseName.substr(0, hyphen);
    MOCKHIC_STAT("Before hyphen _baseName: " << _baseName)
    MOCKHIC_STAT("Return _bdName:" << _bdName)
    _bdName = _baseName;
    MOCKHIC_STAT_RETURN(true, "return true before hyphen _bdName: " << _bdName)
  }

  HIc_Base createBase(const HIc_String& _name = HIc_String(""),
    const HIc_String& _confName = HIc_String(""))
  {
    MOCKHIC_STAT_TIC("HIc_Api", "createBase")
    MOCKHIC_STAT_RETURN(HIc_Base(_name, _confName), "")
  }
};

//--------------------------------------------------------------------------------------------------

void HIc_Init_Standard_Services(const HIc_Api& _api);
void HIc_Init_Standard_Site(const HIc_Api& _api, const HIc_String& _confName);

typedef HIc_Api Api;
typedef std::shared_ptr<HIc_Dictionary> VarTypeDictionary;
}

#define HIC_USE using namespace Mockhic;
#define HERCULE Mockhic
#define HERCULE_NAME Mockhic

#endif // _MOCKHIC_HPP_
